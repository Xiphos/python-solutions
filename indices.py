#!/usr/bin/python
import sys, random
      
def soln(maxInt, maxArrayLen):
    A = [random.randint(0, maxInt) for i in range(random.randint(0,maxArrayLen))]
    print A
    x = -1
    y = -1
    z = -1
    cMax = -1

    for i in range(len(A)):
        x = i
        cMax = A[x]

        y = -1
        z = -1

        for j in range(i+1, len(A)):
            if A[j] > cMax and y == -1:
                cMax = A[j]
                y = j
            elif A[j] > cMax and z == -1 and y != -1:
                z = j
                return (x,y,z)
            elif A[j] < cMax and A[j] > A[x]:
                y = j 
    
    return (x,y,z)

if __name__ == "__main__":
    x,y,z = soln(50, 10)
    print ("{} {} {}".format(x,y,z))