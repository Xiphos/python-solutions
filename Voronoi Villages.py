##Solves the voronoi villages problem
##https://dmoj.ca/problem/ccc18s1
numOfVillages = int(input())
villages = []
for village in range(numOfVillages):
    villages.append(int(input()))

villages.sort()
neighbourHoodSizes = []

for village, V in zip(villages[1:numOfVillages-1:],range(1,numOfVillages-1)):
    leftVillage = villages[V-1]
    rightVillage = villages[V+1]

    leftBound = (village + leftVillage) / 2.0
    rightBound = (village + rightVillage) / 2.0

    neighbourHoodSizes.append(abs(rightBound-leftBound))

print(round(min(neighbourHoodSizes),1))

