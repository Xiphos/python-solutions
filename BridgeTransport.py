###Input is the maximum weight a bridge can handle
###along with the number of cars that must cross the bridge.
###outputs how many cars can cross the bridge
maxWeight = int(input())
numOfCars = int(input())

x = list(map(int, input().split()))

cars = []
for c in range(numOfCars):
    cars.append(int(input()))

def BridgeTransport(maxWeight, carList):
    queue = []
    carsSoFar = 0
    
    for car in carList:
        if sum(queue) > maxWeight:
          return carsSoFar-1
        if len(queue) == 4:
            queue.pop(0)
        queue.append(car)
        carsSoFar += 1
        
    return carsSoFar


print(BridgeTransport(maxWeight, cars))
